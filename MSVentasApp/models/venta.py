from django.db import models
class Venta(models.Model):
    id = models.AutoField('Id', primary_key=True)
    #usuarioid = models.IntegerField('UsuarioId',null=False)
    username = models.CharField('NombreUsuario', max_length = 15)
    vehiculoid = models.IntegerField('VehiculoId',null=False)
    rutaid = models.IntegerField('RutaId',null=False)
    pasajeros = models.IntegerField('Pasajeros',null=False)
    estado = models.CharField('Estado', max_length = 1, default='A')