from django.apps import AppConfig


class MsventasappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'MSVentasApp'
