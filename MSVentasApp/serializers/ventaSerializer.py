from django.db.models import fields
from MSVentasApp.models.venta import Venta
from rest_framework import serializers

class VentaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Venta
        fields = ['id','username','vehiculoid','rutaid','pasajeros','estado']