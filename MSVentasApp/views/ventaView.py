from rest_framework import status, views
from rest_framework.response import Response
from MSVentasApp.models import venta
from MSVentasApp.models.venta import Venta
from MSVentasApp.serializers.ventaSerializer import VentaSerializer
    
class VentaView(views.APIView):
    def get(self, request, pk):
        venta = Venta.objects.get(pk=pk)
        serializer = VentaSerializer(venta)
        
        return Response(serializer.data, 200)

    def post(self, request, *args, **kwargs):
        serializer = VentaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data['id'], status=status.HTTP_201_CREATED)
    
    def delete(self, request, pk):
        try:
            venta = Venta.objects.get(pk=pk)
            venta.delete()
            return Response({"Respuesta": "Venta eliminada."}, 200)
        except:
            return Response({"Respuesta": "Venta no existe."}, 400)
    
    def put(self, request, pk):
        try:
            venta = Venta.objects.get(pk=pk)
            serializer = VentaSerializer(venta, data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
                
            return Response({"Respuesta": "Venta actualizada."}, 200)
        except:
            return Response({"Respuesta": "Venta no existe"}, 400)
        
class VentaAllView(views.APIView):
    def get(self, request):
        listaVentas = Venta.objects.all().order_by('id')
        serializer = VentaSerializer(listaVentas, many=True)
        return Response(serializer.data, 200)
    
class VentaAllUsuarioView(views.APIView):
    def get(self, request, username):
        listaVentas = Venta.objects.filter(username=username)
        serializer = VentaSerializer(listaVentas,many=True)
        
        return Response(serializer.data, 200)
